<!DOCTYPE html>
<html>
<head>
    <title>Ejercicio 7</title>
</head>
<body>

    <p>
        En el ejercicio 7 vammos a usar un sólo fichero ejercicio7.php
    </p>
    <p>
        El formulario va a enviar los datos a si mismo.
        Vamos a formar una lista de jugadores de futbol, cada vez que enviemos guardaremos un nuevo elemento en la lista.
    </p>
    <p>
        PISTA: usa control(es) de tipo hidden.
    </p>

    <?php
        //inicializar list como array vacio
        $list = array();
        //isset --> existe la clave list??
        //empty --> existe y no esta vacia ??
        if (isset($_REQUEST['list'])) {
            $list = $_REQUEST['list'];
        }

        //echo '<pre>';
        //var_dump($list);
        //echo '</pre><hr>';


        if (!empty($_REQUEST['player'])) {
            $list[] = $_REQUEST['player'];
        }

        //echo '<pre>';
        //var_dump($list);
        //echo '</pre><hr>';
    ?>

    <h2>Nuevo jugador</h2>
    <form method="post" action="ejercicio7.php">
        <label>Nuevo jugador</label>
        <input type="text" name="player">

        <?php foreach ($list as $item): ?>
            <input type="hidden" name="list[]" value=
            "<?php echo $item ?>">
        <?php endforeach ?>

        <br>
        <input type="submit" value="Nuevo">
    </form>

    <hr>


    <h2>Lista de jugadores</h2>

    <ul>
    <?php foreach ($list as $item): ?>
        <li><?php echo $item; ?></li>
    <?php endforeach ?>
    </ul>

    <pre>
        <?php var_dump($_REQUEST); ?>
    </pre>
</body>
</html>
