<?php
require_once 'User.php';
 ?>
<!DOCTYPE html>
<html>
<head>
    <title>Lista de usuarios</title>
</head>
<body>
    <header>Encabezado <hr></header>

    <content>
        <h1>Lista de usuarios</h1>

        <table border="style:solid">
            <tr>
                <th>Nombre</th>
                <th>Apellidos</th>
                <th>Edad</th>
                <th>Email</th>
                <th>Operacion</th>
            </tr>
            <?php
                session_start();
                if (!isset($_SESSION['users'])) {
                    $_SESSION['users'] = array();
                }
            ?>
            <?php foreach ($_SESSION['users'] as $key => $user): ?>
                <tr>
                    <td>
                        <?php echo $user->getName() ?>
                    </td>
                    <td>
                        <?php echo $user->getSurname() ?>
                    </td>
                    <td>
                        <?php echo $user->getAge() ?>
                    </td>
                    <td>
                        <?php echo $user->getEmail() ?>
                    </td>
                    <td>
                        <a href="show.php?id=<?php echo $key ?>">Ver</a>
                        <a href="edit.php?id=<?php echo $key ?>">Editar</a>
                        <a href="delete.php?id=<?php echo $key ?>">Borrar</a>
                    </td>
                </tr>


            <?php endforeach ?>
        </table>
        <a href="create.php">Nuevo usuario</a> -
        <a href="empty.php">Vaciar lista de usuarios</a>
    </content>

    <footer><hr>Pie de pagina</footer>
</body>
</html>
