<?php
    require_once('User.php');
    session_start();
    $id = $_GET['id'];
    $user = $_SESSION['users'][$id];
?>
<!DOCTYPE html>
<html>
<head>
    <title>Detalle de usario</title>
</head>
<body>
    <header></header>
    <content>
        <h1>Detalle de usario <?php echo $user ?></h1>
        <ul>
            <li>Nombre: <?php echo $user->getName(); ?></li>
            <li>Apellido: <?php echo $user->getSurname(); ?></li>
            <li>Edad: <?php echo $user->getAge(); ?></li>
            <li>Email: <?php echo $user->getEmail(); ?></li>
        </ul>
    </content>
    <footer></footer>
</body>
</html>
