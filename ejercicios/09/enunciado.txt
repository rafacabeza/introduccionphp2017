Ejercicio 9. (16/10)
- Crear un CRUD sobre un array que contenga la informacion de usuarios. Al no manejar BBDD lo hacemos en una variable de sesion $_SESSION['users']
- Cada usuario lo guardaremos en un objeto de la clase User(name, surname, age, email)
- Vamos a crear varias paginas:
    - Clase User
        User.php
    - Alta de un nuevo usuario:
        create.php --> formulario
        insert.php --> procesar form.
    - Lista de usuarios
        index.php
    - Detalle de usuario
        show.php

    TAREA PARA CASA
    - Borrar usuario
        delete.php
    - Editar usuario
        edit.php
        update.php
